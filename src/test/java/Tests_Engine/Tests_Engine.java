package Tests_Engine;

import org.junit.Test;

import fr.istic.aoc.metronome.command.CMarkMeasure;
import fr.istic.aoc.metronome.command.CMarkTempo;
import fr.istic.aoc.metronome.controller.Controller;
import fr.istic.aoc.metronome.engine.Engine;
import fr.istic.aoc.metronome.view.MetronomeViewImpl;
import junit.framework.TestCase;


public class Tests_Engine extends TestCase {
	private Engine engine;
	
	@Test
	public void test(){
		
		engine = new Engine();
		Controller controller = new Controller();
		controller.setView(new MetronomeViewImpl());
		engine.setMtCommand(new CMarkTempo(controller));
		engine.setMmCommand(new CMarkMeasure(controller));
		assertFalse(engine.isState());
		assertEquals(engine.getBPMe(), 4);
		assertFalse(engine.getClock().isActivated());
		
		
		engine.setBPMi(2);
		assertFalse(engine.isState());
		assertEquals(engine.getBPMe(), 4);
		assertEquals(engine.getBPMi(), 2);
		assertFalse(engine.getClock().isActivated());
		
		
		engine.setState(true);
		assertTrue(engine.isState());
		assertEquals(engine.getBPMe(), 4);
		assertEquals(engine.getBPMi(), 2);
		assertTrue(engine.getClock().isActivated());
		
		
		engine.setState(false);
		assertFalse(engine.isState());
		assertEquals(engine.getBPMe(), 4);
		assertEquals(engine.getBPMi(), 2);
		assertFalse(engine.getClock().isActivated());
	}
}