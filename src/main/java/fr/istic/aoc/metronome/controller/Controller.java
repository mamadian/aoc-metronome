package fr.istic.aoc.metronome.controller;



import fr.istic.aoc.metronome.command.CMarkMeasure;
import fr.istic.aoc.metronome.command.CMarkTempo;
import fr.istic.aoc.metronome.command.Command;
import fr.istic.aoc.metronome.engine.Engine;
import fr.istic.aoc.metronome.view.MetronomeViewImpl;

/**
 * The controller know the view
 * @author ubuntu
 *
 */
@SuppressWarnings("restriction")
public class Controller implements IController {
	
	public Engine engine = new Engine();
	public MetronomeViewImpl view;
	
	public Command cmm= new CMarkMeasure(this);
	public Command cmt= new CMarkTempo(this);
	
	
	public Controller(){
		engine.setMmCommand(cmm);
		engine.setMtCommand(cmt);
		//Number of beats by mesure set to 4 by default
		engine.setBPMe(4);
	}
	
	public void start(){
		if(!engine.isState() && Integer.valueOf(view.tempo.getText()) != 0){
			engine.setBPMi(Integer.valueOf(view.tempo.getText()));
			engine.setState(true);
		}
	}
	
	public void stop(){
		if(engine.isState()){
			engine.setState(false);
		}
	}
	
	public void inc(){
		int measure = engine.getBPMe();
		if(measure< engine.Max_NbTimePerMeasure){
			engine.setBPMe(measure+1);
		}
	}
	
	public void dec(){
		int measure = engine.getBPMe();
		if(measure > engine.Min_NbTimePerMeasure){
			engine.setBPMe(measure-1);
		}
	}
	
	public int getBPMi(){
		return Integer.valueOf(view.tempo.getText());
	}
	
	public void markTempo() {
		view.lightLedTime();
	}

	public void markMeasure() {
		view.lightLedTempo();
	}

	public void updateMolette() {
		if(Integer.valueOf(view.tempo.getText()) != 0){
			engine.setBPMi(Integer.valueOf(view.tempo.getText()));
		}
	}

	public void setView(MetronomeViewImpl view) {
		this.view = view;
	}
}