package fr.istic.aoc.metronome.controller;

/**
 * Design Pattern Command : this is our receiver
 * @author ubuntu
 *
 */
public interface IController {
	public void updateMolette();
	public void start();
	public void stop();
	public void inc();
	public void dec();
	public void markTempo();
	public void markMeasure();
}