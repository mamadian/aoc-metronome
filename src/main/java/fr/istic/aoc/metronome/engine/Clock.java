package fr.istic.aoc.metronome.engine;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import fr.istic.aoc.metronome.command.Command;

public class Clock {
	private Map<Command, Timer> timers = new HashMap<Command, Timer>();
	
	private boolean activated = false;
	
	public void activatePeriodically(final Command command, float periodInSeconds){
		Timer timer = new Timer();
		setActivated(true);
		timers.put(command, timer);
		TimerTask task = new TimerTask() {
			@Override
			public void run() {
				command.execute();
			}
		};
		timer.scheduleAtFixedRate(task, (long) 1, (long) periodInSeconds);
	}
	
	public void activateAfterDelay(final Command command, float delayInSeconds){
		Timer timer = new Timer();
		setActivated(true);
		timers.put(command, timer);
		TimerTask task = new TimerTask() {
			@Override
			public void run() {
				command.execute();

			}
		};
		timer.schedule(task, (long)delayInSeconds);
	}
	
	public void desactivate(Command command){
		Timer timer = timers.get(command);
		if (timer != null) {
			timer.cancel();
			timer.purge();
			timers.remove(command);
			setActivated(false);
		}
	}

	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}	
}