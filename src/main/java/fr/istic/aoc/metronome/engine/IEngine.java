package fr.istic.aoc.metronome.engine;

import fr.istic.aoc.metronome.command.Command;

/**
 * Design Pattern Command : this is our invoker
 * @author ubuntu
 *
 */
public interface IEngine {
	public int getBPMi();
	public int getBPMe();
	public void setBPMe(int BPMe);
	public void setBPMi(int BPMi);
	public void setTickCommand(Command command);
}