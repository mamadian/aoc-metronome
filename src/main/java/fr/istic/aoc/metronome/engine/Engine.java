package fr.istic.aoc.metronome.engine;

import fr.istic.aoc.metronome.command.Command;

public class Engine implements IEngine {
		
		public static final int Min_NbTimePerMeasure = 2;
		public static final int Max_NbTimePerMeasure = 7;
		
		//Correspond to tempo
		private int BPMi;
		//Correspond to beat per mesure
		private int BPMe = 4;
		
		// the clock
		private Clock clock = new Clock();

		// engine status
		private boolean state;
		
		// MarcMeasure  command
		private Command mmCommand;
		
		// MarcTempo  command
		private Command mtCommand;
		
		public Engine(){
			this.state = false;
		}
		
		public int getBPMi() {
			return BPMi;
		}
		
		public void setBPMi(int BPMi) {
			this.BPMi = BPMi;
			
			float time = (float) (60000.0/BPMi);
			if (state){
				clock.desactivate(mtCommand);
				clock.desactivate(mmCommand);
				clock.activatePeriodically(mtCommand, time);
				clock.activatePeriodically(mmCommand, time*BPMe);
			}
		}
		public int getBPMe() {
			return BPMe;
		}
		
		public void setBPMe(int BPMe) {
			this.BPMe = BPMe;
			float time = (float) (60000.0/BPMi);
			if (state){
				clock.desactivate(mtCommand);
				clock.desactivate(mmCommand);
				clock.activatePeriodically(mtCommand, time);
				clock.activatePeriodically(mmCommand, time*BPMe);
			}
		}

		public boolean isState() {
			return this.state;
		}
		
		public void setState(boolean state) {
			this.state = state;
			float time = (float) (60000.0/BPMi);
			if (state){
				clock.activatePeriodically(mtCommand, time);
				clock.activatePeriodically(mmCommand, time*BPMe);
			} else {
				clock.desactivate(mtCommand);
				clock.desactivate(mmCommand);
			}
		}
		
		public Command getMmCommand() {
			return mmCommand;
		}

		public void setMmCommand(Command mmCommand) {
			this.mmCommand = mmCommand;
		}

		public Command getMtCommand() {
			return mtCommand;
		}

		public void setMtCommand(Command mtCommand) {
			this.mtCommand = mtCommand;
		}

		public void setTickCommand(Command command) {
			
		}

		public Clock getClock() {
			return clock;
		}
}