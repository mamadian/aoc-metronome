package fr.istic.aoc.metronome.view;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Timer;
import java.util.TimerTask;

import fr.istic.aoc.metronome.command.CDec;
import fr.istic.aoc.metronome.command.CInc;
import fr.istic.aoc.metronome.command.CStart;
import fr.istic.aoc.metronome.command.CStop;
import fr.istic.aoc.metronome.command.Command;
import fr.istic.aoc.metronome.command.CUpdateTempo;
import fr.istic.aoc.metronome.controller.Controller;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

/**
 * The view doesn't know the controller
 * 
 */
@SuppressWarnings("restriction")
public class MetronomeViewImpl {
	Controller controller= new Controller();
	private Command commandStart =new CStart(controller);
	private Command commandStop = new CStop(controller);
	private Command commandInc = new CInc(controller);
	private Command commandDec = new CDec(controller);
	private Command commandUpdateTempo = new CUpdateTempo(controller);
	
	ClassLoader classLoader = getClass().getClassLoader();
	private String snare = classLoader.getResource("snare.wav").getFile();
	private String beep = classLoader.getResource("beep.wav").getFile();
	
	@FXML
	private Button dec, inc, start, stop;
	@FXML
	private Ellipse led1, led2;
	@FXML
	public Text tempo;
	@FXML
	private Slider slider;
	@FXML
	private Rectangle rectangleTempo;	
	
	@FXML
	public void initialize(){
		slider.setValue(100);
		commandUpdateTempo.setView(this);
		slider.valueProperty().addListener(new ChangeListener<Number>() {
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				tempo.setText(String.valueOf(newValue.intValue()));
				commandUpdateTempo.execute();
			}
		});
		slider.setMin(0);
		slider.setMax(300);
		slider.setBlockIncrement(10);
	}
	
	@FXML
	public void handleStartButton(ActionEvent ac) {
		commandStart.setView(this);
		commandStart.execute();
	}
	
	@FXML
	protected void handleStopButton(ActionEvent ac) {
		commandStop.setView(this);
		commandStop.execute();
	}
	
	@FXML
	protected void handleIncButton(ActionEvent ac) {
		commandInc.setView(this);
		commandInc.execute();
	}
	
	@FXML
	protected void handleDecButton(ActionEvent ac) {
		commandDec.setView(this);
		commandDec.execute();
	}
	
	public void lightLedTempo(){
		led1.setFill(Color.RED);
		playSon(snare);
		unlightAfterDelay(led1);
	}
	
	public void lightLedTime(){
		led2.setFill(Color.ORANGE);
		playSon(beep);
		unlightAfterDelay(led2);
	}
	
	/**
	 * Put off the lef after a delay to simulate a beep
	 * @param led
	 */
	private void unlightAfterDelay(final Ellipse led){
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				Platform.runLater(new Runnable() {
					public void run() {
						led.setFill(Color.DODGERBLUE);
					}
				});
			}
			//TODO : calculer le délai en fonction du tempo
		},(long) ((slider.getValue()-300)*-1)
		);
	}
	/**
	 * allow to Play the son
	 * @param Url
	 */
	private  void playSon(String url){
		try 
		{ 
		    InputStream in = new FileInputStream(url);
		    AudioStream as = new AudioStream(in);
		    AudioPlayer.player.start(as);
		}
		catch (IOException e) 
		{
		    System.err.println(e);  
		}
	}
}