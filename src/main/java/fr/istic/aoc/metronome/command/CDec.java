package fr.istic.aoc.metronome.command;

import fr.istic.aoc.metronome.controller.Controller;
import fr.istic.aoc.metronome.view.MetronomeViewImpl;

public class CDec implements Command {
	
	private Controller controller;
	
	private MetronomeViewImpl view;
	
	public CDec(Controller controller) {
		this.controller = controller;
	}
	public void execute() {
		controller.dec();
	}
	
	public MetronomeViewImpl getView() {
		return view;
	}
	public void setView(MetronomeViewImpl view) {
		this.view = view;
	}	
}