package fr.istic.aoc.metronome.command;

import fr.istic.aoc.metronome.controller.Controller;
import fr.istic.aoc.metronome.view.MetronomeViewImpl;

/**
 * Design Pattern Command : this is one concrete Command
 * @author ubuntu
 *
 */
public class CMarkTempo implements Command{
	
	Controller controller;
	
	private MetronomeViewImpl view;
	
	public CMarkTempo(Controller controller){
		this.controller = controller;
	}
	
	public void execute() {
		controller.markTempo();
	}

	public MetronomeViewImpl getView() {
		return view;
	}

	public void setView(MetronomeViewImpl view) {
		this.view = view;
	}
}