package fr.istic.aoc.metronome.command;

import fr.istic.aoc.metronome.controller.Controller;
import fr.istic.aoc.metronome.view.MetronomeViewImpl;

/**
 * Design Pattern Command : this is one concrete Command
 * @author ubuntu
 *
 */
public class CMarkMeasure implements Command {
	private Controller controller;

	private MetronomeViewImpl view;
	
	public CMarkMeasure(Controller controller){
		this.controller = controller;
	}
	
	public void execute() {
		controller.markMeasure();
	}

	public MetronomeViewImpl getView() {
		return view;
	}

	public void setView(MetronomeViewImpl view) {
		this.view = view;
	}
}