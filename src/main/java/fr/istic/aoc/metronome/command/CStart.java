package fr.istic.aoc.metronome.command;

import fr.istic.aoc.metronome.controller.Controller;
import fr.istic.aoc.metronome.view.MetronomeViewImpl;

public class CStart implements Command {
	
	private Controller controller;
	
	private MetronomeViewImpl view;
	
	public CStart(Controller controller) {
		this.controller =  controller;
	}
	public void execute() {
		controller.setView(view);
		controller.start();
	}
	
	public MetronomeViewImpl getView() {
		return view;
	}
	public void setView(MetronomeViewImpl view) {
		this.view = view;
	}	
}