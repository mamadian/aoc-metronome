package fr.istic.aoc.metronome.command;

import fr.istic.aoc.metronome.view.MetronomeViewImpl;

/**
 * Design Pattern Command : this is the Command
 * @author ubuntu
 *
 */
public interface Command {
	
	final MetronomeViewImpl view = null;
	
	public void execute();
	
	public MetronomeViewImpl getView();
	
	public void setView(MetronomeViewImpl view);
}