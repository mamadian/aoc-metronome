package fr.istic.aoc.metronome.command;

import fr.istic.aoc.metronome.controller.Controller;
import fr.istic.aoc.metronome.view.MetronomeViewImpl;

public class CStop implements Command {
	
	private Controller controller;
	
	private MetronomeViewImpl view;
	
	public CStop(Controller controller) {
		this.controller = controller;
	}
	public void execute() {
		controller.stop();
	}

	public MetronomeViewImpl getView() {
		return view;
	}
	public void setView(MetronomeViewImpl view) {
		this.view = view;
	}	
}